
const fastify = require('./server');

const initializeServer = async () => {
  fastify.register(require('@fastify/env'), require('./config/env').options);
  await fastify.after(); // we need to wait for the envs to be loaded before we can use the env config in further steps

  fastify.register(require('@fastify/swagger'), require('./config/swagger').options(fastify.config.PORT));

  fastify.register(
      require('sequelize-fastify'),
      {
        instance: 'db',
        sequelizeOptions: {
          dialect: fastify.config.DB_DIALECT_NAME, /* one of 'mysql' | 'mariadb' | 'postgres' | 'mssql' */
          database: fastify.config.DB_NAME,
          username: fastify.config.DB_USER,
          password: fastify.config.DB_PASS,
          options: {
            host: fastify.config.DB_HOST,
            port: fastify.config.DB_PORT,
          },
        },
      },
  );
  await fastify.after(); // need to wait after db initialization to check for db connection and syncing models to the db

  // check db connection is successful
  await fastify.db.authenticate();
  console.log(
      'Database connection is successfully established..',
  );

  require('./routes').forEach((route) => {
    fastify.route(route);
  });

  await fastify.ready(); // waits for all fastify registered plugins to load.
};

const syncDb = async () => {
  require('./models')();
  await fastify.db.sync(); // This creates the table if it doesn't exist (and does nothing if it already exists)
};

const start = async () => {
  try {
    await initializeServer();
    await syncDb();
    await fastify.listen(fastify.config.PORT);
    fastify.swagger();
    fastify.log.info(`listening on ${fastify.server.address().port}`);
  } catch (err) {
    fastify.log.error(err);
    fastify.close();
    process.exit(1);
  }
};
start();


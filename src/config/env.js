const configSchema = {
  type: 'object',
  required: [],
  properties: {
    PORT: {
      type: 'integer',
      default: 3000,
    },
    DB_DIALECT_NAME: {
      type: 'string',
      default: 'postgres',
    },
    DB_PORT: {
      type: 'integer',
      default: 5432,
    },
    DB_NAME: {
      type: 'string',
      default: 'inventory',
    },
    DB_HOST: {
      type: 'string',
      default: 'localhost',
    },
    DB_USER: {
      type: 'string',
      default: 'postgres',
    },
    DB_PASS: {
      type: 'string',
      default: '',
    },
  },
};

exports.options = {
  confKey: 'config', // optional, default: 'config'
  schema: configSchema,
  dotenv: true, // will read .env in root folder
};


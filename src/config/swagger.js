const options = (port) => {
  return {
    routePrefix: '/docs',
    exposeRoute: true,
    swagger: {
      info: {
        title: 'Products API',
        description: `Building a blazing fast REST API with Node.js, PostgresDB, Fastify and Swagger
        for product inventory`,
        version: '1.0.0',
      },
      host: `localhost:${port}`,
      schemes: ['http'],
      consumes: ['application/json'],
      produces: ['application/json'],
    },
  };
};

exports.options = options;

const {
  ReasonPhrases,
  StatusCodes,
} = require('http-status-codes');
const Category = require('../models/category');
const ApiError = require('../utils/error');

exports.getCategories = async (req) => {
  try {
    const {parentId = ''} = req.query;
    if (parentId) {
      return Category.findAll({
        where: {
          parentId,
        },
      });
    }
    return Category.findAll();
  } catch (error) {
    throw new ApiError(StatusCodes.INTERNAL_SERVER_ERROR, ReasonPhrases.INTERNAL_SERVER_ERROR);
  }
};

exports.createCategory = async (req) => {
  try {
    return Category.create({
      ...req.body,
    });
  } catch (error) {
    throw new ApiError(StatusCodes.INTERNAL_SERVER_ERROR, ReasonPhrases.INTERNAL_SERVER_ERROR);
  }
};

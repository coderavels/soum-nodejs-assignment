const {
  ReasonPhrases,
  StatusCodes,
} = require('http-status-codes');
const {parse} = require('tinyduration');
const date = require('date-and-time');
const Product = require('../models/product');
const ApiError = require('../utils/error');

exports.getProducts = async (req) => {
  try {
    return Product.findAll({
      where: {
        ...req.query,
      },
    });
  } catch (error) {
    throw new ApiError(StatusCodes.INTERNAL_SERVER_ERROR, ReasonPhrases.INTERNAL_SERVER_ERROR);
  }
};

exports.updateProduct = async (req) => {
  try {
    const productId = req.params.id;
    return Product.update({
      ...req.body,
    }, {
      where: {
        id: productId,
      },
    });
  } catch (error) {
    throw new ApiError(StatusCodes.INTERNAL_SERVER_ERROR, ReasonPhrases.INTERNAL_SERVER_ERROR);
  }
};

exports.createProduct = async (req) => {
  try {
    const data = req.body;
    const {validityDuration} = data;
    const durationObj = parse(validityDuration);
    let validityEnd = new Date();
    validityEnd = date.addYears(validityEnd, durationObj.years);
    validityEnd = date.addMonths(validityEnd, durationObj.months);
    validityEnd = date.addDays(validityEnd, 7 * durationObj.weeks + durationObj.days);
    validityEnd = date.addHours(validityEnd, durationObj.hours);
    validityEnd = date.addMinutes(validityEnd, durationObj.minutes);
    validityEnd = date.addSeconds(validityEnd, durationObj.seconds);
    const product = Product.build({...data, validityEnd});
    return product.save();
  } catch (error) {
    if (error.message === 'Invalid duration') {
      throw new ApiError(StatusCodes.BAD_REQUEST, `${ReasonPhrases.BAD_REQUEST}: invalid validityDuration ${error}`);
    }
    throw new ApiError(StatusCodes.INTERNAL_SERVER_ERROR, ReasonPhrases.INTERNAL_SERVER_ERROR);
  }
};

const fastify = require('../server');
const Ajv = require('ajv');

const ajv = new Ajv({
  removeAdditional: true,
  useDefaults: true,
  coerceTypes: 'array',
  allErrors: true,
});

fastify.setValidatorCompiler(({schema, method, url, httpPart}) => {
  return ajv.compile(schema);
});

fastify.addSchema({
  $id: 'productCreateSchema',
  type: 'object',
  additionalProperties: false,
  properties: {
    name: {
      type: 'string',
      description: 'name of the product',
    },
    price: {
      type: 'number',
      description: 'price of the product',
    },
    images: {
      type: 'array',
      description: 'list of image urls for the product',
      items: {
        type: 'string',
      },
    },
    mainImage: {
      type: 'string',
      description: 'main image of the product',
    },
    categoryId: {
      type: 'integer',
      description: 'category of the product',
    },
    validityDuration: {
      type: 'string',
      description: `duration for which the product is going to be valid after listing. Format: ISO-8601. 
        Example: "P1DT12H"`,
    },
  },
  required: ['name', 'price', 'images', 'mainImage', 'categoryId', 'validityDuration'],
});

const productController = require('../controllers/product');
const categoryController = require('../controllers/category');
const {VALID_PRODUCT_STATUS} = require('../models/product');

const routes = [
  {
    method: 'GET',
    url: '/api/v1/categories',
    handler: categoryController.getCategories,
    schema: {
      query: {
        type: 'object',
        properties: {
          parentId: {
            type: 'string',
            description: 'if provided, only the child categories of this parentId would be returned.',
          },
        },
        additionalProperties: false,
      },
    },
  },
  {
    method: 'POST',
    url: '/api/v1/categories',
    handler: categoryController.createCategory,
    schema: {
      body: {
        $ref: 'categoryCreateSchema#',
      },
    },
  },
  {
    method: 'POST',
    url: '/api/v1/products',
    handler: productController.createProduct,
    schema: {
      body: {
        $ref: 'productCreateSchema#',
      },
    },
  },
  {
    method: 'GET',
    url: '/api/v1/products',
    handler: productController.getProducts,
    schema: {
      query: {
        type: 'object',
        properties: {
          status: {
            type: 'string',
            enum: VALID_PRODUCT_STATUS,
            description: 'if provided, the products in response would be filtered by status',
          },
        },
        additionalProperties: false,
      },
    },
  },
  {
    method: 'PATCH',
    url: '/api/v1/products/:id',
    handler: productController.updateProduct,
    schema: {
      body: {
        description: 'fields to be updated for the product',
        type: 'object',
        properties: {
          status: {
            type: 'string',
            enum: VALID_PRODUCT_STATUS,
            description: 'the status to update the product to',
          },
        },
        required: ['status'],
        additionalProperties: false,
      },
    },
  },
];

module.exports = routes;

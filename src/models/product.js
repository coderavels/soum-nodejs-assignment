/* eslint-disable new-cap */
const {DataTypes, Deferrable} = require('sequelize');
const server = require('../server');
const Category = require('./category');

const VALID_PRODUCT_STATUS = ['draft', 'draft_deleted', 'available', 'deleted',
  'expired', 'reserved', 'sold', 'returned'];

const Product = server.db.define( 'Product', {
  // Model attributes are defined here
  name: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  price: {
    type: DataTypes.FLOAT,
  },
  images: {
    type: DataTypes.ARRAY(DataTypes.STRING),
  },
  mainImage: {
    type: DataTypes.STRING,
  },
  categoryId: {
    type: DataTypes.INTEGER,
    allowNull: false,
    references: {
      model: Category,
      key: 'id',
      // With PostgreSQL, it is optionally possible to declare when to check the foreign key constraint, passing the Deferrable type.
      deferrable: Deferrable.INITIALLY_IMMEDIATE,
    },
  },
  status: {
    type: DataTypes.ENUM({
      values: VALID_PRODUCT_STATUS,
    }),
    defaultValue: VALID_PRODUCT_STATUS[0],
  },
  validityDuration: {
    type: DataTypes.STRING, // iso 8601 duration string
    allowNull: false,
  },
  validUntil: {
    type: DataTypes.DATE, // stored as exact date so that its easier for apis and workers to fetch non-expired products
    allowNull: false,
  },
}, {
  timestamps: true,
  indexes: [
    {
      unique: true,
      fields: ['name'],
    },
    {
      fields: ['status'],
    },
  ],
});

module.exports = Product;
exports.VALID_PRODUCT_STATUS = VALID_PRODUCT_STATUS;

/* eslint-disable new-cap */
const {DataTypes} = require('sequelize');
const server = require('../server');

const Category = server.db.define( 'Category', {
  name: {
    type: DataTypes.STRING,
    allowNull: false,
  },
}, {
  timestamps: false, // don't add created_at, updated_at timestamps
});

Category.hasOne(Category, {as: 'parent', foreignKey: 'parentId'});

module.exports = Category;

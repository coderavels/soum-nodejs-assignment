const initializeModels = () => {
  require('./product');
  require('./category');
};

module.exports = initializeModels;

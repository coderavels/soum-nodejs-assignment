# soum-nodejs-assignment

Nodejs assignment for soum coding interview round.

## Problem Statement
The problem statement is described in details in PDF file located at `docs/Node.js_assessment.pdf` in the repo.

## ERD
![ERD](/docs/erd.png)

The ERD draw.io xml file can be found under /docs folder.

## Toolkit
1. [fastify](https://www.fastify.io/): node.js web framework
2. [swagger](https://github.com/fastify/fastify-swagger): api documentation
3. [sequelize](https://sequelize.org/): ORM
3. [postgres](https://www.npmjs.com/package/pg): primary datastore
4. [eslint](https://eslint.org/): linting
5. [dotenv](https://www.npmjs.com/package/dotenv): configuration management
6. [nodemon](): watch the file changes and reload local server during development

## Setup
1. setup postgres: [official installation tutorial](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&cad=rja&uact=8&ved=2ahUKEwjd2uHj08f3AhWLzTgGHdqvAmMQFnoECAcQAQ&url=https%3A%2F%2Fwww.postgresql.org%2Fdocs%2Fcurrent%2Ftutorial-install.html&usg=AOvVaw26IKUSaTySL-pwX3O86tgm)
2. install all project packages: `npm install`
3. configure environment variables: Check `Configuration` section below.

## Configuration
The project uses [dotenv](https://www.npmjs.com/package/dotenv) for configuration management. Create a file `.env` at the root of the project to provide the configuration variables. Check the `.env.example` file to see all the options that can be configured.

## Linting
The project uses [google-eslint-config](https://github.com/google/eslint-config-google) for eslint configuratin with some custom rules set. Check the `.eslintrc.json` file for the complete configuration.

## Scripts
1. `start`: starts the server on local
2. `test`: runs the unit tests

## API Documentation.

Swagger documentation for all the apis is hosted at `<host>/docs` api route.

